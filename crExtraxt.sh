#File selection
if [ "$1" == "-h" ];then
	echo "Usage: crExtract [-n filename.apk]"
	exit
elif [ "$1" == "-n" ];then
	if [ $2 ]; then
		filename=$2
	else 
	    echo "You need to insert a filename"
		exit
	fi
else
	echo "Give me the clash royale filename"
	read filename
fi
if [ ! -f $filename ]; then
    echo "File not found!"
	exit
fi
#Unzipping
echo "Unzipping crash royale apk"
unzip -o -q $filename -d "extracted"
#Decrypting
echo "Decrypting csv files"
mkdir decrypted
for path in $(find extracted/assets -name '*.csv')
	do
		f=$(echo "$path" | rev | cut -d"/" -f1 | rev)
		(
			dd if=$path bs=1 count=9 status=none
			dd if=/dev/zero bs=1 count=4 status=none
			dd if=$path bs=1 skip=9 status=none
		) | lzma -dc -f > "decrypted/"$f
done
echo "All the decrypted file are stored in the decrypted folder"
#Delete partial files
rm -r extracted